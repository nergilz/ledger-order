package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"orderer/config_order"
	"orderer/config_peer"
	"os"
	"os/signal"
	"sync"

	"github.com/0LuigiCode0/library/logger"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"
	"google.golang.org/grpc"
)

const (
	network   = "tcp"
	orderAddr = ":7001"
	peer1Addr = ":9000"
	peer2Addr = ":9001"
)

// todo получить данные с пиров, адрес и порт

type OrderServer struct {
	config_order.OrdererServiceServer

	address      string
	peerAddrList []string
	connections  map[string]*PeerClient

	wg        sync.WaitGroup
	rw        sync.RWMutex
	ctx       context.Context
	CtxCancel context.CancelFunc
	log       *logger.Logger
	db        *leveldb.DB
}

type PeerClient struct {
	cp   config_peer.ContentPeerClient
	conn *grpc.ClientConn
}

type ResponceModel struct {
	Key   string          `json:"key"`
	Value json.RawMessage `json:"value"`
}

func main() {
	// init order
	order := &OrderServer{
		address:      orderAddr,
		connections:  make(map[string]*PeerClient),
		peerAddrList: make([]string, 0),
		log:          logger.InitLogger(""),
	}
	order.ctx, order.CtxCancel = context.WithCancel(context.Background())
	order.peerAddrList = append(order.peerAddrList, peer1Addr, peer2Addr)

	// init DB
	var err error
	order.db, err = order.InitLevelDB()
	if err != nil {
		order.log.Errorf("cannot init leveldb: %v", err)
	}
	defer order.db.Close()
	order.log.Service("create database")

	// run server
	listener, err := net.Listen(network, order.address)
	if err != nil {
		order.log.Fatalf("fatal listen on orderer: %v", err)
	}
	order.wg.Add(1)
	go order.RunServer(listener)
	order.log.Servicef("start order grpc server: %v", order.address)

	// get peers connection
	for _, addr := range order.peerAddrList {
		order.wg.Add(1)
		go order.InitPeer(addr)
		order.log.Servicef("peer connect: %v", addr)
	}

	// stop service
	c := make(chan os.Signal, 9)
	signal.Notify(c, os.Interrupt)
	<-c
	order.CtxCancel()
	listener.Close()
	order.wg.Wait()
}

func (order *OrderServer) RunServer(listener net.Listener) {
	defer order.wg.Done()

	grpcServer := grpc.NewServer()
	config_order.RegisterOrdererServiceServer(grpcServer, order)

	if err := grpcServer.Serve(listener); err != nil {
		order.log.Warningf("failed order grpc server: %v", err)
	}
}

func (order *OrderServer) InitPeer(addr string) {
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		order.log.Warningf("order cannot dial peer on: %v", err)
	}
	defer conn.Close()
	defer order.wg.Done()

	peerClient := config_peer.NewContentPeerClient(conn)
	peerCli := &PeerClient{
		cp:   peerClient,
		conn: conn,
	}
	order.SetConn(addr, peerCli)
	<-order.ctx.Done()
}

// OrderCalc переопределяет функцию из config_order, запрос в peer
func (order *OrderServer) OrderCalc(ctx context.Context, in *config_order.EchoRequest) (*config_order.EchoResponse, error) {
	ch := make(chan interface{}, len(order.peerAddrList))
	wg := sync.WaitGroup{}

	for _, addr := range order.peerAddrList {
		client, err := order.GetConn(addr)
		if err != nil {
			order.log.Warningf("bad peer: %v, %v", addr, err)
			continue
		}
		wg.Add(1)
		go client.RunPeer(ctx, &wg, in, ch)
		order.log.Infof("run peer & get msg")
	}
	wg.Wait()

	var echoMsg *config_order.EchoResponse
	for v := range ch {
		switch v := v.(type) {
		case error:
			return &config_order.EchoResponse{}, v.(error)
		case *config_order.EchoResponse:
			if result := v.Result; echoMsg == nil {
				echoMsg = &config_order.EchoResponse{
					Result: result,
				}
			} else if echoMsg.Result != result {
				return echoMsg, fmt.Errorf("ожидался ответ: %v, пришел: %v", echoMsg.Result, result)
			}
		}
		if len(ch) == 0 {
			close(ch)
			break
		}
	}
	if echoMsg == nil {
		return &config_order.EchoResponse{}, fmt.Errorf("error: nil pointer in responce")
	}
	return echoMsg, nil
}

func (peer *PeerClient) RunPeer(ctx context.Context, wg *sync.WaitGroup, in *config_order.EchoRequest, ch chan interface{}) {
	defer wg.Done()
	echoMsg := &config_order.EchoResponse{}
	echoReq := &config_peer.Request{
		X:        in.X,
		Y:        in.Y,
		Operator: in.Operator,
	}
	peerMsg, err := peer.cp.PeerServerCalculate(ctx, echoReq)
	if err != nil {
		ch <- err
		return
	}
	echoMsg.Result = peerMsg.Result
	ch <- echoMsg
}

// helper
func (order *OrderServer) GetConn(addr string) (*PeerClient, error) {
	order.rw.Lock()
	defer order.rw.Unlock()
	if conn, ok := order.connections[addr]; ok {
		return conn, nil
	}
	return nil, fmt.Errorf("cannot get connection addr: %v", addr)
}

func (order *OrderServer) SetConn(addr string, conn *PeerClient) {
	order.rw.Lock()
	defer order.rw.Unlock()
	order.connections[addr] = conn
}

func (order *OrderServer) DelConn(addr string) {
	order.rw.Lock()
	defer order.rw.Unlock()
	delete(order.connections, addr)
}

// store
func (order *OrderServer) InitLevelDB() (*leveldb.DB, error) {
	db, err := leveldb.OpenFile("levelDatabase", nil)
	if err != nil {
		return db, fmt.Errorf("cannot open leveldb: %v", err)
	}
	return db, nil
}

func (order *OrderServer) SearchByPrefix(prefix []byte) ([]*ResponceModel, error) {
	res := make([]*ResponceModel, 0)
	iter := order.db.NewIterator(util.BytesPrefix(prefix), nil)
	for iter.Next() {
		byPrefix := new(ResponceModel)
		byPrefix.Key = string(iter.Key())
		byPrefix.Value = iter.Value()
		res = append(res, byPrefix)
	}
	iter.Release()
	if err := iter.Error(); err != nil {
		return nil, err
	}
	return res, nil
}
