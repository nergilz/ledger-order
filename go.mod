module orderer

go 1.16

require (
	github.com/0LuigiCode0/library v0.0.0-20210419181544-cc9bdd4a019e // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	golang.org/x/net v0.0.0-20210414194228-064579744ee0 // indirect
	golang.org/x/sys v0.0.0-20210415045647-66c3f260301c // indirect
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/genproto v0.0.0-20210415145412-64678f1ae2d5 // indirect
	google.golang.org/grpc v1.37.0 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
